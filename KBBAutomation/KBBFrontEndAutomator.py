'''
KBB Front End Data Compilation Automator
Author : Samidh Chatterjee
Date : August 18, 2016 ( First Edition)

The GUI and the install_and_import part were taken from Mr. Gautam Krishna's (gautam.krishna@360i.com)Basket Analyzer code.

'''
import pip
import time

def install_and_import(package):

    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        pip.main(['install', package])
    finally:
        globals()[package] = importlib.import_module(package)

install_and_import('argparse')
install_and_import('pandas')
install_and_import('configparser')
install_and_import('itertools')
install_and_import('logging')
install_and_import('sys')
install_and_import('tkinter')


import pandas as pd
import configparser
import itertools
import logging
import datetime


from tkinter import *
from tkinter import messagebox
from collections import Counter
#Directories and variables
root=Tk()
root.title('Data Compiler')
root.geometry('{}x{}'.format(250,125))
logging.basicConfig(level=logging.INFO)

month_dict = {'1' : 'January',
              '2' : 'February',
              '3' : 'March',
              '4' : 'April',
              '5' : 'May',
              '6' : 'June',
              '7' : 'July',
              '8' : 'August',
              '9' : 'September',
              '10' : 'October',
              '11' : 'November',
              '12' : 'December'}

# Find reporting week (previous Monday)
def get_reporting_week(ymd):

    date0 = datetime.date(int(ymd[0]), int(ymd[1]), int(ymd[2]))
    prev_monday = date0 - datetime.timedelta(date0.weekday())
    return prev_monday

#Filter aggregated data according to partner and DCM
def process_columns(dcmMapPartner,
                    fileTypes,
                    col,
                    type='map'):
    item = '.'.join([type,col])
    files = dcmMapPartner[item].unique()
    try:
        assert (all(file in fileTypes for file in files))
    except AssertionError:
        logging.warning('One or more filetype not present in config') #'n/a'
        logging.warning(files)
    if col not in dcmMapPartner.columns:
        dcmMapPartner[col] = 0

    for fType in files:
        if fType == 'Partner':
            try:
                dcmMapPartner.loc[dcmMapPartner[item] == fType, col] = dcmMapPartner[dcmMapPartner[item] == fType]['part.' + col]
            except KeyError as error:
                logging.error('%s is not a column in Partner data', error)
                dcmMapPartner.loc[dcmMapPartner[item] == fType, col] = 0
        elif fType == 'DCM':
            try:
                if item == 'map.Spend':
                    #if dcmMapPartner[dcmMapPartner[item] == fType]['Rate Type'] == 'CPM':
                    dcmMapPartner.loc[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPM'), col] = \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPM')]['Rate']* \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPM')]\
                                                                        ['Impressions']*1.0/1000
                    dcmMapPartner.loc[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPCV'), col] = \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPCV')]['Video Completions'] * \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPCV')]['Rate']
                    dcmMapPartner.loc[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPE'), col] = \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPE')]['Rate'] * \
                        dcmMapPartner[(dcmMapPartner[item] == fType) & (dcmMapPartner['Rate Type'] == 'CPE')]['Impressions']
                else:
                    dcmMapPartner.loc[dcmMapPartner[item] == fType, col] = dcmMapPartner[dcmMapPartner[item] == fType][col]

            except KeyError as error:
                logging.error('%s is not a column in DCM data', error)
                dcmMapPartner.loc[dcmMapPartner[item] == fType, col] = 0
        else:
            dcmMapPartner.loc[dcmMapPartner[item] == fType, col] = 0

    return dcmMapPartner

# Merge DCM with LookUp file
def mergeDCMLookUp(dcmDf, lookUpFile,
                   PlacementSheet,
                   ThemeSheet,
                   SIOMIDSheet):
    lookUpFile = pd.ExcelFile(lookUpFile[1:-1])
    lookUpPlacement = lookUpFile.parse(PlacementSheet)
    lookUpTheme = lookUpFile.parse(ThemeSheet)
    lookUpSIOMID = lookUpFile.parse(SIOMIDSheet)
    dcmDf['Placement'] = dcmDf['Placement'].apply(lambda x: x.rstrip().lstrip())
    lookUpPlacement['Placement'] = lookUpPlacement['Placement'].apply(lambda x: x.rstrip().lstrip())

    dcmDf['Creative'] = dcmDf['Creative'].apply(lambda x: x.rstrip().lstrip())
    lookUpTheme['Creative'] = lookUpTheme['Creative'].apply(lambda x: x.rstrip().lstrip())
    lookUpSIOMID['Creative'] = lookUpSIOMID['Creative'].apply(lambda x: x.rstrip().lstrip())
    try:
        mergedDf = dcmDf.merge(lookUpPlacement, how = 'left', on='Placement')\
                         .merge(lookUpTheme, how = 'left', on='Creative')\
                         .merge(lookUpSIOMID, how = 'left', on='Creative')
    except KeyError as error:
        logging.error('%s is not a column', error)
        sys.exit(1)
    return mergedDf
# Read map file into a data frame and rename tobeMapped cols to map.cols
def processMapFile(mapFile, toBeMapped):
    mappingDf = pd.read_excel(mapFile[1:-1])
    oldMapCols = list(mappingDf.columns)
    newMapCols = ['.'.join(['map',x]) if x in toBeMapped else x for x in oldMapCols]
    mappingDf.columns = newMapCols
    return (mappingDf, newMapCols)

#Read partner file into a data frame and rename tobeMapped cols to part.cols
def processPartnerFiles(partnerFiles,
                       partnerSheetNames,
                       toBeMapped):
    dfList = []
    for filename in partnerFiles:
        partnerExcel = pd.ExcelFile(filename[1:-1])
        dfSheets = [partnerExcel.parse(sheet) for sheet in partnerSheetNames[filename]]
        dfList += [partnerExcel.parse(sheet) for sheet in partnerSheetNames[filename]]
    partnerDf = pd.concat(dfList)
    oldPartCols = list(partnerDf.columns)
    newPartCols = ['.'.join(['part', x]) if x in toBeMapped else x for x in oldPartCols]
    partnerDf.columns = newPartCols
    return (partnerDf, newPartCols)

#Getoutput
def run() :
    start = time.time()
    config = configparser.ConfigParser()
    config.read('KBBConf.ini')
    confSections = config.sections()

    try:
        assert('DCMDoc' in confSections)
        dcmFile    =  config['DCMDoc']['name']
    except AssertionError as error:
        logging.error('DCMFile section or subsection is absent in config')
        sys.exit(1)

    try:
        assert('PartnerDoc' in confSections)
    except AssertionError as error:
        logging.error('PartnerDoc section or subsection is absent in config')
        sys.exit(1)

    try:
        assert('MappingDoc' in confSections)
    except AssertionError as error:
        logging.error('MappingDoc section or subsection is absent in config', )
        sys.exit(1)

    try:
        assert('LookUpDoc' in confSections)
    except AssertionError as error:
        logging.error('LookUpDoc section or subsection is absent in config', )
        sys.exit(1)

    try:
        assert('OutputDoc' in confSections)
    except AssertionError as error:
        logging.error('OutputDoc section or subsection is absent in config')
        sys.exit(1)

    try:
        dcmSheetName  =  config['DCMDoc']['sheetname']
        skipRows   =  int(config['DCMDoc']['skiprows'])
        skipFooter =  int(config['DCMDoc']['skipfooter'])
        noOfPartFiles = int(config['PartnerDoc']['noOfFiles'])
        partFilenames = [config['PartnerDoc']['name'+str(idx)] for idx in range(1,noOfPartFiles+1)]
		#print(partFilenames)
		#sys.exit(0)
        partnerSheetNames  =  {x[0] : config['PartnerDoc']['sheetname'+str(x[1])].split(",")\
                                          for x in zip(partFilenames, range(1,noOfPartFiles+1))}
        mapFile = config['MappingDoc']['name']
        mapSheetname = config['MappingDoc']['name']
        lookUpFile = config['LookUpDoc']['name']
        PlacementSheet = config['LookUpDoc']['PlacementSheet']
        ThemeSheet = config['LookUpDoc']['ThemeSheet']
        SIOMIDSheet = config['LookUpDoc']['SIOMIDSheet']
        OutName = config['OutputDoc']['name']
        toBeMapped = config['MappingDoc']['mappedCols'].split(',')
        fileTypes  = config['MappingDoc']['fileTypes'].split(',')
    except KeyError as error:
        logging.error('KeyError : %s is not in config', error)
        sys.exit(1)
    # Read DCM data
    dcmDf = pd.read_csv(dcmFile[1:-1], engine='python', skiprows=skipRows, skip_footer=skipFooter)
    dcmDf['Date'] = pd.to_datetime(dcmDf['Date'])
    # Read Look Up file and merge with DCM 
    mergedDf = mergeDCMLookUp(dcmDf,
                              lookUpFile,
                              PlacementSheet,
                              ThemeSheet,
                              SIOMIDSheet)
    #Maintain same columnn name in Map and rename Columns
    [mappingDf, newMapCols] = processMapFile(mapFile, toBeMapped)
    #Concatenate all partner sheets
    [partnerDf, newPartCols] = processPartnerFiles(partFilenames,
                                   partnerSheetNames,
                                   toBeMapped)
    rqdColsMap    = [i for i in itertools.chain(['Placement'],
                                                itertools.filterfalse(lambda x : not x.startswith('map'), newMapCols))]
    rqdColsPart   = [i for i in itertools.chain(['Placement', 'Date'],
                                                itertools.filterfalse(lambda x : not x.startswith('part'), newPartCols))]
    dcmMap = mergedDf.merge(mappingDf[rqdColsMap], how = 'left',
                            on='Placement')
    dcmMapPartner = dcmMap.merge(partnerDf[rqdColsPart],
                                 how='left',
                                 on=['Date','Placement'],
                                 )
    for col in toBeMapped:
            dcmMapPartner = process_columns(dcmMapPartner,
                                            fileTypes,
                                            col)
    dcmMapPartner['Weighted'] = dcmMapPartner['Weight'].multiply(dcmMapPartner['Global : global counter: View-through Conversions'])
    dcmMapPartner.loc[dcmMapPartner['Spend'] == 0, 'Rate'] = 0
    dcmMapPartner['Reporting Week'] = dcmMapPartner['Date'].apply(lambda x : get_reporting_week(x.strftime('%Y-%m-%d')\
                                                                            .split('-')).strftime('%m/%d/%Y'))
    dcmMapPartner['Month'] = dcmMapPartner['Reporting Week'].apply(lambda x: month_dict[str(int(x.split('/')[0]))])
    dcmMapPartner['Date'] = dcmMapPartner['Date'].apply(lambda x : x.strftime('%m/%d/%Y'))
    dcmMapPartner.sort_values(by=['Date','Placement'], inplace=True)
    dcmMapPartner['SpendProportion'] = 0
    dcmMapPartner['ClickProportion'] = 0
    dcmMapPartner['GCTConversionProportion'] = 0
    count = 0
    dcmMapPartner.fillna(0, inplace=True)
    dcmMapPartner = dcmMapPartner.drop_duplicates()
    #Final adjustment of SpendProportion
    for group in dcmMapPartner.groupby(by=['Date','Placement']).groups:
        #print(group)
        count += 1
        print('Group ' + str(count) + ' processed')
        if sum((dcmMapPartner[(dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1])]\
                        ['map.Spend']).isin(['DCM'])) != 0:
            dcmMapPartner.loc[
                (dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1]), 'SpendProportion'] = \
                dcmMapPartner[
                    (dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1])]['Spend']
            continue
        spendDict = Counter(dcmMapPartner[(dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1])]['Spend'])
        #uniqueSpends = list((dcmMapPartner[(dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1])]['Spend']).unique())
        condition = (dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1])
        #print(spendDict.items())
        impressionSumDict = {k : sum(dcmMapPartner[(dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1]) & \
                                 (dcmMapPartner['Spend'] == k)]['Impressions']) for k in spendDict.keys()}

        impressionSum = sum(dcmMapPartner[condition]['Impressions'])

        #print(dcmMapPartner[(dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1])][['Spend', 'Impressions']])
        if impressionSum != 0 :
            dcmMapPartner.loc[(dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1]),'SpendProportion'] =\
            dcmMapPartner[(dcmMapPartner['Date']== group[0]) & (dcmMapPartner['Placement'] == group[1])][['Spend', 'Impressions']]\
                .apply(lambda x : x['Spend']*x['Impressions']*1.0/impressionSumDict[x['Spend']], axis = 1)

            if group[1].find('TABOOLA') != -1:
                dcmMapPartner.loc[
                        (dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1]), 'ClickProportion'] = \
                        dcmMapPartner[(dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1])]\
                        [['Clicks','Impressions']].apply(lambda x : x['Clicks']*x['Impressions']*1.0/impressionSum, axis = 1)
                dcmMapPartner.loc[
                            (dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1]),\
                                                'GCTConversionProportion'] = \
                            dcmMapPartner[(dcmMapPartner['Date'] == group[0]) & (dcmMapPartner['Placement'] == group[1])]\
                                        [['Global : global counter: Click-through Conversions', 'Impressions']]\
                            .apply(lambda x : x['Global : global counter: Click-through Conversions']*x['Impressions']\
                                              *1.0/impressionSum, axis = 1)

    logging.info('Cleaning extra columns and writing to main file....')
    toBeDropped = [x for x in dcmMapPartner.columns if x.startswith('map') or x.startswith('part')]
    dcmMapPartner = dcmMapPartner.drop(toBeDropped, axis=1)
    #dcmMapPartner.fillna(0, inplace=True)
    #uniqData = dcmMapPartner.drop_duplicates()
    dcmMapPartner.to_excel(OutName[1:-1])
    Final_file_name = OutName
    Msg_display = "Data Compiled\nFilename: " + Final_file_name
    end = time.time()
    logging.info('Elapsed time : %s seconds', (end - start))
    messagebox.showinfo(title="Data Compilation Done", message=Msg_display)
    root.destroy()

GetBA_opt_button=Button(root, text='Run Auto Compiler',
                        command=run, fg='white',
                        bg='red',width=15,
                        height=1,
                        font=("Helvetica", 12,"bold"))
GetBA_opt_button.place(x=20, y=15)
root.mainloop()
